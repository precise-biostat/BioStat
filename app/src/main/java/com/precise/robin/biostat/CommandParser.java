package com.precise.robin.biostat;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.precise.robin.biostat.ConsoleActivity.compileDataToJson;
import static com.precise.robin.biostat.ConsoleActivity.createProgressDialog;
import static com.precise.robin.biostat.ConsoleActivity.getProgressDialogProgress;
import static com.precise.robin.biostat.ConsoleActivity.mProgressDialog;
import static com.precise.robin.biostat.ConsoleActivity.updateFinishedProgressDialog;
import static com.precise.robin.biostat.ConsoleActivity.updateProgressDialog;
/*
*  Command parser for the file transfer protocol. Enforces the sequence of command and parses the data as an entire file.*
* */
public class CommandParser {
    private Context context;
    private int fileCnt;
    private int fileTarget;
    public static Set<String> acceptedFileFormat = new HashSet<>(Arrays.asList("HRM_FILE", "LIN_ACC_FILE"));
    Map<String, Integer> charCntMap;
    Map<String, Boolean> cmdState;
    private String currFile;
    Handler mHandler = new Handler();

    public CommandParser(Context context) {
        this.context = context;

        // Initialize variables
        resetState();
    }

    // Helper function that resets all of the variables
    private void resetState() {
        fileCnt = 0;
        fileTarget = 0;
        cmdState = new HashMap<>();
        charCntMap = new HashMap<>();
        cmdState.put("start_transfer", false);
        cmdState.put("preamble", false);
        cmdState.put("file_start", false);
        cmdState.put("file_transfer", false);
        cmdState.put("file_stop", false);
        currFile = "";
    }

    // Helper function that checks if the current state matches the parameters entered
    private boolean stateValid(boolean start_transfer, boolean preamble, boolean file_start, boolean file_transfer, boolean file_stop) {
        return cmdState.get("start_transfer") == start_transfer && cmdState.get("preamble") == preamble && cmdState.get("file_start") == file_start && cmdState.get("file_transfer") == file_transfer && cmdState.get("file_stop") == file_stop;
    }

    // Debug function that returns the string of the current state of the file protocol
    private String getStates() {
        return "start_transfer: " + String.valueOf(cmdState.get("start_transfer")) + ", " +
                "preamble: " + String.valueOf(cmdState.get("preamble")) + ", " +
                "file_start: " + String.valueOf(cmdState.get("file_start")) + ", " +
                "file_transfer: " + String.valueOf(cmdState.get("file_transfer")) + ", " +
                "file_stop: " + String.valueOf(cmdState.get("file_stop"));
    }

    public void parse(List<CommandObject> commandObjectList) {

        for (CommandObject commandObject : commandObjectList) {
            Log.i("CommandParser", "Received command: " + commandObject.toString());
            switch (commandObject.getCommand()) {

                case "start_transfer":
                    if (stateValid(false, false, false, false, false) || stateValid(true, true, true, true, true)) {
                        // Create the download dialog and reset all the variables
                        createProgressDialog(context.getString(R.string.Download_remote_data_dialog));
                        resetState();
                        cmdState.put("start_transfer", true);
                    } else {
                        throw new IllegalStateException("Wrong command sequence for " + commandObject.getCommand());
                    }

                    break;
                case "preamble":

                    if (stateValid(true, false, false, false, false)) {
                        // Enforce dialog
                        if (mProgressDialog != null) {

                            List<String> files = new ArrayList<>(commandObject.getParams());
                            files.remove("cmd");

                            // Get count of files being transferred
                            fileTarget = files.size();

                            // Map character count to file type
                            for (String file : files) {
                                charCntMap.put(file, commandObject.getCount(file));
                            }

                            // Check if file type is recognized, if not then log warning
                            for (String s : charCntMap.keySet()) {

                                if (!acceptedFileFormat.contains(s)) {
                                    Log.i("CommandParser", "Parsing unrecognized file format: " + s);
                                }
                            }

                        }

                        cmdState.put("preamble", true);
                    } else {
                        throw new IllegalStateException("Wrong command sequence for " + commandObject.getCommand());
                    }

                    break;
                case "file_start":
                    if (stateValid(true, true, false, false, false) || stateValid(true, true, true, true, true)) {
                        // Update the download dialog with the data from the preamble
                        updateProgressDialog(0, charCntMap.get(commandObject.getFile()), "Transferring: " + commandObject.getFile());
                        currFile = commandObject.getFile();

                        fileCnt++;
                        cmdState.put("file_start", true);
                        cmdState.put("file_transfer", false);
                        cmdState.put("file_stop", false);
                    } else {
                        throw new IllegalStateException("Wrong command sequence for " + commandObject.getCommand());
                    }


                    break;
                case "file_transfer":
                    if (stateValid(true, true, true, false, false) || stateValid(true, true, true, true, false)) {
                        //Log.i("CommandParser", commandObject.getData(currFile));

                        // Parse the data through the JSonParser and cache them for compilation
                        String data = commandObject.getData(currFile);
                        final JSonParser parser = new JSonParser(data);
                        if (parser.isData()) {
                            final List<DataObject> jsonList = parser.getJsonList();
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {

                                    ConsoleActivity.cacheData(jsonList);

                                }
                            });
                        }

                        // Update progress bar by char count
                        updateProgressDialog(getProgressDialogProgress() + data.length());

                        cmdState.put("file_transfer", true);
                    } else {
                        throw new IllegalStateException("Wrong command sequence for " + commandObject.getCommand() + getStates());
                    }


                    break;
                case "file_stop":
                    if (stateValid(true, true, true, true, false)) {
                        // Check if all the files have been sent
                        if (fileCnt == fileTarget) {
                            Toast.makeText(context, "Finished data transfer", Toast.LENGTH_SHORT).show();

                            // Update finished progress dialog
                            updateFinishedProgressDialog(context.getString(R.string.Download_finish_dialog_text));

                            // Compile JSON to dataWiz format
                            compileDataToJson(context);
                        }

                        cmdState.put("file_stop", true);
                    } else {
                        throw new IllegalStateException("Wrong command sequence for " + commandObject.getCommand());
                    }


                    break;

                default:
                    Log.i("CommandParser", "Received unknown command, unable to parse: " + commandObject.toString());
            }
        }


    }
}
