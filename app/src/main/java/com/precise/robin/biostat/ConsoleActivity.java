/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation and/or
 *       other materials provided with the distribution.
 *     * Neither the name of Samsung Electronics Co., Ltd. nor the names of its contributors may be used to endorse or
 *       promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.precise.robin.biostat;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonWriter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class ConsoleActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static boolean isBusy = false;
    public static boolean isRemote = false;
    public static ConsoleActivity mContext;
    public static ConsoleService mConsumerService = null;
    private static MessageAdapter mMessageAdapter;
    private static File hrmCacheFile, linAccCacheFile;
    private static boolean mIsBound = false;
    private static JsonParser jsonParser = new JsonParser();
    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private static Handler mHandler = new Handler();
    private static JsonObject statusViewJson = new JsonObject();
    public static ProgressDialog mProgressDialog;
    private static Runnable mCycleManager = new Runnable() {
        private int cnt = 0;

        @Override
        public void run() {
            int newRate;
            switch ((cnt++) % 4) {
                case 0:
                    newRate = 10;
                    break;
                case 1:
                    newRate = 50;
                    break;
                case 2:
                    newRate = 1;
                    break;
                case 3:
                    newRate = 100;
                    break;
                default:
                    newRate = 5;
                    break;
            }
            changeRefreshRate(newRate);
            mHandler.postDelayed(mCycleManager, 10000);
        }
    };
    private final ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mConsumerService = ((ConsoleService.LocalBinder) service).getService();
            updateTextView("onServiceConnected");

            setBtnEnable("Connect", true);
            setBtnEnable("Disconnect", false);
            setBtnEnable("Start", false);
            setBtnEnable("Stop", false);
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            mConsumerService = null;
            mIsBound = false;
            updateTextView("onServiceDisconnected");

            setBtnEnable("Connect", true);
            setBtnEnable("Disconnect", false);
            setBtnEnable("Start", false);
            setBtnEnable("Stop", false);
        }
    };

    // Delete file from cache directory with matching prefix by regex
    public static void clearCache(final String prefix, Context mContext) {
        // Select the cache directory
        final File folder = mContext.getCacheDir();
        // Filter all the file with correct cache prefix
        final File[] files = folder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(final File dir,
                                  final String name) {
                return name.matches(prefix + ".*\\.tmp");
            }
        });
        // Remove all the selected files
        for (final File file : files) {
            if (!file.delete()) {
                Log.e("Exception", "Can't remove " + file.getAbsolutePath());
            }
        }

    }

    // Write to file line by line, used for caching raw data to tmp file
    private static void writeToFile(String data, File file) {
        OutputStreamWriter outputStreamWriter = null;
        try {
            // Create the writer
            outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file, true));

            // Split the data by new line characters and write them line by line to file
            String[] lines = data.split("\n");
            for (String line : lines) {
                outputStreamWriter.write(line + "\n");
                //Log.i("DEBUG", "Wrote to " + file.getAbsolutePath() + ": " + line);
            }

        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        } finally {
            // Close the writer
            if (outputStreamWriter != null) {
                try {
                    outputStreamWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // Read from cache file and  write to JSON file as the output, parsed and printed in prettyprint
    private static void writeToFile(File cache, File file, Context mContext) {
        JsonWriter jsonWriter = null;
        BufferedReader bufferedReader = null;
        try {
            // Create the JSON writer
            jsonWriter = new JsonWriter(new BufferedWriter(new FileWriter(file)));

            // Enable pretty printing to file
            jsonWriter.setIndent("    ");

            // Create the buffered reader
            bufferedReader = new BufferedReader(new FileReader(cache));

            String receiveString;
            // Constructing JSON structure according to DataWiz outputs
            jsonWriter.beginObject();
            jsonWriter.name(getHash(mContext));
            jsonWriter.beginObject();
            jsonWriter.name("data");
            jsonWriter.beginArray();

            while ((receiveString = bufferedReader.readLine()) != null) {
                gson.toJson(jsonParser.parse(receiveString), jsonWriter);
                //Log.i("Writing to JSON file", receiveString);
            }

            jsonWriter.endArray();
            jsonWriter.endObject();
            jsonWriter.endObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Close the reader
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                    Log.i("DEBUG", "CLOSING READING STREAM");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // Close the writer
            if (jsonWriter != null) {
                try {
                    jsonWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // Cache the raw data to cache directory for temporary storage
    public static void cacheData(List<DataObject> dataObjectList) {

        // Check if temp file available and taking in data
        if (dataObjectList != null && hrmCacheFile != null && linAccCacheFile != null && isBusy) {
            File target = null;

            // Check with dataObject type as to which temporary file we are targeting
            if (dataObjectList.get(0) instanceof HrmDataObject) {
                target = hrmCacheFile;
            } else if (dataObjectList.get(0) instanceof LinAccDataObject) {
                target = linAccCacheFile;
            } else {
                Log.e("Exception", "cannot cache data, unknown dataObject type");
            }

            // Cache the data, object by object
            if (target != null) {
                updateObjectStatus(dataObjectList.get(0));
                for (DataObject dataObject : dataObjectList) {
                    writeToFile(dataObject.toString(), target);


                }
            } else {
                Log.e("Exception", "target file not found when caching");
            }
        } else {

            Log.e("Exception", "Unable to cache data");
        }

    }

    private static void updateObjectStatus(DataObject dataObject) {
        updateStatusView("Watch Charging", Boolean.toString(dataObject.getBatteryState()));
        updateStatusView("Watch Battery", Double.toString(dataObject.getBatteryLevel()));
        updateStatusView("Sample Rate", Integer.toString(dataObject.getRefreshRate()));
        updateStatusView("Count", Integer.toString(dataObject.getCount()));
        if (dataObject instanceof HrmDataObject) {
            updateStatusView("Step Counter", Integer.toString(((HrmDataObject) dataObject).getPedomTotalStep()));
        }
    }

    // Wrapper for normal compilation of data in a normal session
    public static void compileDataToJson(Context mContext) {
        compileDataToJson(hrmCacheFile, linAccCacheFile, false, mContext);
    }

    // Compiles data from cache to JSON in designated directory, with option to mark if compiling a recovered session (adds recover tag to filename)
    public static void compileDataToJson(final File hrmCacheFile, final File linAccCacheFile, boolean isRecovered, final Context mContext) {
        if (hrmCacheFile != null && linAccCacheFile != null) {
            // disable further data intakes
            isBusy = false;
            Log.i("DEBUG", "compiling");
            Toast.makeText(mContext, "Compiling data to JSON", Toast.LENGTH_SHORT).show();

            try {

                // Construct file directory and file name
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
                String time = getTime();

                String hrmDir = prefs.getString("key_hrm_dir", "");
                String linAccDir = prefs.getString("key_lin_dir", "");

                // Check if the path still exist, in case directory is removed during session
                File hrmDirFile = new File(hrmDir);
                File linAccDirFile = new File(linAccDir);
                if (!hrmDirFile.isDirectory()) {
                    hrmDirFile.mkdirs();
                }
                if (!linAccDirFile.isDirectory()) {
                    linAccDirFile.mkdirs();
                }

                String hrmPathName;
                String linAccPathName;
                if (!isRecovered) {
                    hrmPathName = hrmDir + File.separator + mContext.getString(R.string.hrm_file_header) + mContext.getString(R.string.file_separator) + time + mContext.getString(R.string.json_extension);
                    linAccPathName = linAccDir + File.separator + mContext.getString(R.string.linAcc_file_header) + mContext.getString(R.string.file_separator) + time + mContext.getString(R.string.json_extension);
                } else {
                    hrmPathName = hrmDir + File.separator + mContext.getString(R.string.hrm_file_header) + mContext.getString(R.string.file_separator) + mContext.getString(R.string.recovered_filename_tag) + mContext.getString(R.string.file_separator) + time + mContext.getString(R.string.json_extension);
                    linAccPathName = linAccDir + File.separator + mContext.getString(R.string.linAcc_file_header) + mContext.getString(R.string.file_separator) + mContext.getString(R.string.recovered_filename_tag) + mContext.getString(R.string.file_separator) + time + mContext.getString(R.string.json_extension);
                }

                final File hrmFile = new File(hrmPathName);
                final File linAccFile = new File(linAccPathName);

                Log.i("DEBUG", "hrmPath: " + hrmPathName);
                Log.i("DEBUG", "linAccPath: " + linAccPathName);

                // Create the actual file
                hrmFile.createNewFile();
                linAccFile.createNewFile();

                // Write JSON data to file
                Thread thread1 = new Thread() {
                    public void run() {
                        writeToFile(hrmCacheFile, hrmFile, mContext);
                    }
                };
                Thread thread2 = new Thread() {
                    public void run() {
                        writeToFile(linAccCacheFile, linAccFile, mContext);
                    }
                };
                Log.i("DEBUG", "Starting writing JSON for HRM");
                thread1.start();
                Log.i("DEBUG", "Starting writing JSON for lin acc");
                thread2.start();

                thread1.join();
                Log.i("DEBUG", "Finished writing JSON for HRM to: " + hrmFile.getAbsolutePath());
                Toast.makeText(mContext, "Finished writing JSON for HRM to: " + hrmFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();

                thread2.join();
                Log.i("DEBUG", "Finished writing JSON for lin acc" + linAccFile.getAbsolutePath());
                Toast.makeText(mContext, "Finished writing JSON for lin acc to: " + linAccFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Toast.makeText(mContext, "Finishing JSON compilation", Toast.LENGTH_SHORT).show();

            // Automatically clear cache if regular session, option to manually delete if in a recovery session
            if (!isRecovered) {
                clearCache(mContext.getString(R.string.sessionCache_prefix), mContext);
            }
        } else {
            throw new IllegalArgumentException("Null cache file passed in to compileDataToJson");
        }

    }

    // Generate the hash value for JSON
    private static String getHash(Context mContext) {
        return mContext.getString(R.string.dummyHash);
    }

    // Helper function that returns the timestamp, used for filenames
    public static String getTime() {
        char separator = mContext.getString(R.string.file_separator).charAt(0);
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
        int day = Calendar.getInstance().get(Calendar.DATE);
        int hour = Calendar.getInstance().get(Calendar.HOUR);
        int minute = Calendar.getInstance().get(Calendar.MINUTE);
        int second = Calendar.getInstance().get(Calendar.SECOND);
        return String.valueOf(year) + separator + month + separator + day + separator + hour + separator + minute + separator + second;
    }

    // Add string to list view
    public static void addMessage(String data) {
        mMessageAdapter.addMessage(new Message(data));
    }

    // Update the status textView
    public static void updateTextView(final String str) {
        TextView textView = mContext.findViewById(R.id.tvStatus);
        textView.setText(str);
    }

    // Update the UserID textView
    public static void updateUserIdView(final String str) {
        TextView textView = mContext.findViewById(R.id.UserId);
        textView.setText(str);
    }

    public static void updateUserIdView() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        String userID = prefs.getString("patient_name_key", "");
        TextView textView = mContext.findViewById(R.id.UserId);
        if (userID.equals("")) {
            userID = "NOT SET";
            textView.setTextColor(ResourcesCompat.getColor(mContext.getResources(), R.color.warning_text_color, null));
        } else {
            textView.setTextColor(ResourcesCompat.getColor(mContext.getResources(), R.color.default_text_color, null));
        }
        updateUserIdView("Patient ID: " + userID);

    }

    public static void updateStatusView(String key, String value) {
        statusViewJson.addProperty(key, value);
        updateStatusView();
    }

    public static void updateStatusView() {
        TextView textView = mContext.findViewById(R.id.statusView);
        String payLoad = "";
        String separator = ", ";
        for (String k : statusViewJson.keySet()) {
            if (payLoad.equals("")) {
                payLoad += k + ": " + statusViewJson.get(k);
            } else {
                String newPayload = k + ": " + statusViewJson.get(k);

                if (payLoad.substring(payLoad.lastIndexOf("\n") + 1).length() + newPayload.length() < 50) {
                    payLoad += separator + newPayload;
                } else {
                    payLoad += "\n" + newPayload;
                }

            }
        }
        textView.setText(payLoad);
    }

    private static void clearStatusView() {
        statusViewJson = new JsonObject();
        updateStatusView();
    }

    // Enable / disable button for proper control flow
    public static void setBtnEnable(String id, boolean state) {
        Button connectionBtn = mContext.findViewById(R.id.buttonConnect);
        Button disconnectBtn = mContext.findViewById(R.id.buttonDisconnect);
        Button startSensorsBtn = mContext.findViewById(R.id.buttonStart);
        Button stopSensorsBtn = mContext.findViewById(R.id.buttonStop);
        Button startRemoteBtn = mContext.findViewById(R.id.buttonStartRemote);
        Button stopRemoteBtn = mContext.findViewById(R.id.buttonStopRemote);
        switch (id) {
            case "Connect":
                connectionBtn.setEnabled(state);
                break;
            case "Disconnect":
                disconnectBtn.setEnabled(state);
                break;
            case "Start":
                startSensorsBtn.setEnabled(state);
                break;
            case "Stop":
                stopSensorsBtn.setEnabled(state);
                break;
            case "StartRemote":
                startRemoteBtn.setEnabled(state);
                break;
            case "StopRemote":
                stopRemoteBtn.setEnabled(state);
                break;
            default:
                break;

        }
    }

    // Presses the connect and start button to restart the connection
    public static void performReconnect(final boolean isNewSession) {


        final Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                buttonConnectAction();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Button startSensorsBtn = mContext.findViewById(R.id.buttonStart);
                        if (startSensorsBtn.isEnabled()) {
                            buttonStartAction(isNewSession);
                        }
                    }
                }, 2000);
            }
        }, 2000);
    }

    // Helper function that checks if bluetooth is disabled, if so then enable
    private static void bluetoothEnable() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();

        }
        updateBluetoothStatus();
    }

    private static void updateBluetoothStatus() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null) {
            updateStatusView("Bluetooth", mBluetoothAdapter.isEnabled() ? "On" : "Off");

        } else {
            updateStatusView("Bluetooth", "false");
        }
    }

    // Constructs and returns a new temp file with proper prefix header
    private static File getTempFile(String header) {
        File outputDir = mContext.getCacheDir();
        try {
            Log.i("DEBUG", "CREATING NEW TEMP FILE");
            return File.createTempFile(mContext.getString(R.string.sessionCache_prefix) + mContext.getString(R.string.file_separator) + header + "_", null, outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i("DEBUG", "FAIL TO CREATE TEMP FILE");
        return null;
    }

    // Action when Connect button is pressed
    private static void buttonConnectAction() {

        clearStatusView();
        // Check if bluetooth is enabled, if not then enable it
        bluetoothEnable();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        isRemote = prefs.getBoolean("remoteSessionRunning", false);
        isBusy = isRemote;

        if (mIsBound && mConsumerService != null) {
            mConsumerService.findPeers();
        }


    }

    // Sends payload to Tizen wearable
    private static boolean sendData(String payLoad) {
        if (mIsBound && mConsumerService != null) {
            return mConsumerService.sendData(payLoad);
        }
        return false;
    }

    // Action when Start Sensors button is pressed
    private static void buttonStartAction(boolean isNewSession) {
        if (mIsBound && mConsumerService != null) {
            // Getting command payload values from shared preferences
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
            int refreshRate = prefs.getInt("refresh_rate_key", 1);
            boolean isLocallyStoring = prefs.getBoolean("key_watch_local_storage", false);
            String hrmPrefix = prefs.getString("key_hrm_prefix", "HRM");
            String linAccPrefix = prefs.getString("key_lin_acc_prefix", "LIN_ACC");

            // Initialize the payload variables
            String payLoad, cmd;
            CommandPayload commandPayload;


            Toast.makeText(mContext, "isNewSession: " + Boolean.toString(isNewSession), Toast.LENGTH_SHORT).show();
            Log.i("DEBUG", "isNewSession: " + Boolean.toString(isNewSession));


            // Create payload by session preference
            if (isNewSession) {
                // Create new temp file
                hrmCacheFile = getTempFile(mContext.getString(R.string.hrm_cache_header));
                linAccCacheFile = getTempFile(mContext.getString(R.string.linAcc_cache_header));

                // Store the cache file directories for recovery
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("hrmCacheFileDir_key", hrmCacheFile.getAbsolutePath());
                editor.putString("linAccCacheFileDir_key", linAccCacheFile.getAbsolutePath());
                editor.apply();

                // Create the payload
                cmd = mContext.getString(R.string.payload_start_cmd);

                commandPayload = new CommandPayload(cmd);

                commandPayload.addProperty("cmd", cmd);
                commandPayload.addProperty("refreshRate", refreshRate);
                commandPayload.addProperty("localStorage", isLocallyStoring);
                commandPayload.addProperty("hrmPrefix", hrmPrefix);
                commandPayload.addProperty("linAccPrefix", linAccPrefix);
                commandPayload.addProperty("restartDuration", ConsoleService.CONNECTION_RETRY_LIMIT * ConsoleService.RESTART_INTERVAL);

                payLoad = commandPayload.toString();
                //payLoad = "{\"cmd\": \"start\", \"refreshRate\": " + refreshRate + ", \"localStorage\": " + isLocallyStoring + ", \"hrmPrefix\": \"" + hrmPrefix + "\", \"linAccPrefix\": \"" + linAccPrefix + "\"}";
            } else {
                // Check if the cache file to append the session to still exists

                hrmCacheFile = new File(prefs.getString("hrmCacheFileDir_key", null));
                linAccCacheFile = new File(prefs.getString("linAccCacheFileDir_key", null));

                if (hrmCacheFile == null || linAccCacheFile == null) {
                    Log.i("DEBUG", "NULL cache files from sharedprefs");
                    throw new IllegalStateException("Previous Cache Files cannot be found");
                }

                cmd = mContext.getString(R.string.payload_resume_cmd);
                commandPayload = new CommandPayload(cmd);
                payLoad = commandPayload.toString();

            }

            // Enable data intake
            isBusy = true;

            // Reset graphs for new sessions
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    GraphActivity.clearAllSeries();
                }
            });

            boolean isDemoDynamicSampling = prefs.getBoolean("demo_dynamic_rate_key", false);

            if (sendData(payLoad)) {
                setBtnEnable("Connect", false);
                setBtnEnable("Disconnect", false);
                setBtnEnable("Start", false);
                setBtnEnable("Stop", true);

                // Check if the session is a demo
                if (isDemoDynamicSampling) {
                    startCycle();
                }

            } else {
                Toast.makeText(mContext, R.string.ConnectionAlreadyDisconnected, Toast.LENGTH_SHORT).show();
            }
        }

    }

    private static void startCycle() {
        mCycleManager.run();
    }

    private static void stopCycle() {
        mHandler.removeCallbacks(mCycleManager);
    }

    // Action when Disconnect button is pressed
    private static void buttonDisconnectAction() {
        if (mIsBound && mConsumerService != null) {
            if (!mConsumerService.closeConnection()) {
                updateTextView("Disconnected");
                Toast.makeText(mContext, R.string.ConnectionAlreadyDisconnected, Toast.LENGTH_SHORT).show();

                mMessageAdapter.clear();

                setBtnEnable("Connect", true);
                setBtnEnable("Disconnect", false);
                setBtnEnable("Start", false);
                setBtnEnable("StartRemote", false);
                setBtnEnable("Stop", false);
                setBtnEnable("StopRemote", false);

            }

        }

    }

    // Action when StopSensors button is pressed
    private static void buttonStopAction() {
        String cmd = mContext.getString(R.string.payload_stop_cmd);
        stopAction(cmd, true);


    }

    private static void stopAction(String cmd, boolean isCompilingJson) {
        if (mIsBound && mConsumerService != null) {

            // Remove cache file directories from sharedprefs
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
            SharedPreferences.Editor editor = prefs.edit();
            editor.remove("hrmCacheFileDir_key");
            editor.remove("linAccCacheFileDir_key");
            editor.apply();


            CommandPayload commandPayload = new CommandPayload(cmd);
            String payLoad = commandPayload.toString();
            //String payLoad = "{\"cmd\": \"stop\"}";

            boolean isDemoDynamicSampling = prefs.getBoolean("demo_dynamic_rate_key", false);

            if (sendData(payLoad)) {
                setBtnEnable("Connect", false);
                setBtnEnable("Disconnect", true);
                setBtnEnable("Start", true);
                setBtnEnable("StartRemote", true);
                setBtnEnable("Stop", false);
                setBtnEnable("StopRemote", false);

                // Check if the session is a demo
                if (isDemoDynamicSampling) {
                    stopCycle();
                }

                if (isCompilingJson) {
                    compileDataToJson(mContext);
                }

            } else {
                Toast.makeText(mContext, R.string.ConnectionAlreadyDisconnected, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static void buttonStopRemoteAction() {
        isRemote = false;
        String cmd = mContext.getString(R.string.payload_stop_local_cmd);


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        // Create new temp file
        hrmCacheFile = getTempFile(mContext.getString(R.string.hrm_cache_header));
        linAccCacheFile = getTempFile(mContext.getString(R.string.linAcc_cache_header));

        // Store the cache file directories for recovery
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("hrmCacheFileDir_key", hrmCacheFile.getAbsolutePath());
        editor.putString("linAccCacheFileDir_key", linAccCacheFile.getAbsolutePath());
        editor.putBoolean("remoteSessionRunning", false);
        editor.apply();

        stopAction(cmd, false);
    }

    private static void buttonStartRemoteAction() {
        if (mIsBound && mConsumerService != null) {
            // Getting command payload values from shared preferences
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
            int refreshRate = prefs.getInt("refresh_rate_key", 1);
            String hrmPrefix = prefs.getString("key_hrm_prefix", "HRM");
            String linAccPrefix = prefs.getString("key_lin_acc_prefix", "LIN_ACC");

            // Initialize the payload variables
            String payLoad, cmd;
            CommandPayload commandPayload;


            // Create the payload
            cmd = mContext.getString(R.string.payload_start_local_cmd);

            commandPayload = new CommandPayload(cmd);

            commandPayload.addProperty("cmd", cmd);
            commandPayload.addProperty("refreshRate", refreshRate);
            commandPayload.addProperty("localStorage", true);
            commandPayload.addProperty("hrmPrefix", hrmPrefix);
            commandPayload.addProperty("linAccPrefix", linAccPrefix);
            commandPayload.addProperty("restartDuration", ConsoleService.CONNECTION_RETRY_LIMIT * ConsoleService.RESTART_INTERVAL);

            payLoad = commandPayload.toString();

            // Enable data intake
            isBusy = true;
            isRemote = true;

            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("remoteSessionRunning", true);
            editor.apply();

            // Reset graphs for new sessions
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    GraphActivity.clearAllSeries();
                }
            });

            boolean isDemoDynamicSampling = prefs.getBoolean("demo_dynamic_rate_key", false);

            if (sendData(payLoad)) {
                setBtnEnable("Connect", false);
                setBtnEnable("Disconnect", true);
                setBtnEnable("StartRemote", false);
                setBtnEnable("StopRemote", true);

                // Check if the session is a demo
                if (isDemoDynamicSampling) {
                    startCycle();
                }

            } else {
                Toast.makeText(mContext, R.string.ConnectionAlreadyDisconnected, Toast.LENGTH_SHORT).show();
            }
        }
    }

    // Function for changing the refresh rate of the current session
    private static void changeRefreshRate(int newRefreshrate) {
        if (mIsBound && mConsumerService != null && isBusy) {
            if (newRefreshrate < 1 || newRefreshrate > 100) {
                throw new IllegalArgumentException();
            }
            String cmd = mContext.getString(R.string.payload_changeRR_cmd);
            CommandPayload commandPayload = new CommandPayload(cmd);
            commandPayload.addProperty("refreshRate", newRefreshrate);
            String payLoad = commandPayload.toString();
            sendData(payLoad);
            GraphActivity.changeColor(newRefreshrate);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        setContentView(R.layout.activity_console);

        mMessageAdapter = new MessageAdapter();
        ((ListView) findViewById(R.id.lvMessage)).setAdapter(mMessageAdapter);

        // Bind service
        mIsBound = bindService(new Intent(ConsoleActivity.this, ConsoleService.class), mConnection, Context.BIND_AUTO_CREATE);

        // Initialize buttons
        setBtnEnable("Connect", true);
        setBtnEnable("Disconnect", false);
        setBtnEnable("Start", false);
        setBtnEnable("StartRemote", false);
        setBtnEnable("Stop", false);
        setBtnEnable("StopRemote", false);

        // Action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Check for permission, give warning if not granted
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(this, "Storage Permission not granted, cannot store data to local storage!\nRestart app or go to Settings -> Apps to grant permission", Toast.LENGTH_SHORT).show();
        }

        // Check if remaining cache files can be recovered from prior forced closure of app
        if (getIntent().getBooleanExtra("isRecovery", false)) {
            recoverCachedFiles(mContext);
            SettingsActivity.disablePrefixEditing = false;
        } else {
            // Remove the rest of the cache, reset it to proper state
            clearCache(mContext.getString(R.string.sessionCache_prefix), mContext);
        }

        // Check if this is a app restart, if so try to reconnect
        if (getIntent().getBooleanExtra("isReconnect", false)) {
            Toast.makeText(this, "Performing reconnection procedures", Toast.LENGTH_SHORT).show();
            performReconnect(false);
        }


    }

    // Creates and show a ProgressDialog with the message
    public static void createProgressDialog(String message) {
        // Maintain only 1 dialog
        if (mProgressDialog != null) {
            dismissProgressDialog();
        }
        // Create new dialog
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage(message);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

    }

    // Dismiss the current ProgressDialog if existed
    public static void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    // Makes the dialog cancelable and changes message
    public static void updateFinishedProgressDialog(String message) {
        if (mProgressDialog != null) {
            mProgressDialog.setMessage(message);
            mProgressDialog.setProgress(mProgressDialog.getMax());
            mProgressDialog.setCancelable(true);
        }
    }

    // Update current ProgressDialog with the progress values
    public static void updateProgressDialog(Integer progress, Integer max, String message) {
        if (mProgressDialog != null) {
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(max);
            mProgressDialog.setProgress(progress);
            mProgressDialog.setMessage(message);
        }
    }

    public static void updateProgressDialog(Integer progress) {
        if (mProgressDialog != null) {
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setProgress(progress);
        }
    }

    public static int getProgressDialogProgress() {
        if (mProgressDialog != null) {
            return mProgressDialog.getProgress();
        } else {
            return -1;
        }
    }

    @Override
    protected void onResume() {
        updateBluetoothStatus();
        updateLogVisibility();
        updateButtonVisibility();
        updateUserIdView();
        super.onResume();
    }

    private void updateButtonVisibility() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        boolean isRemoteSession = prefs.getBoolean("remote_session_key", false);
        LinearLayout liveSessionLayout = findViewById(R.id.liveSessionButtonBar);
        LinearLayout remoteSessionLayout = findViewById(R.id.remoteSessionButtonBar);
        liveSessionLayout.setVisibility(!isRemoteSession ? LinearLayout.VISIBLE : LinearLayout.GONE);
        remoteSessionLayout.setVisibility(isRemoteSession ? LinearLayout.VISIBLE : LinearLayout.GONE);
    }

    private void updateLogVisibility() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        boolean isVerbose = prefs.getBoolean("verbose_mode_key", false);
        ListView listView = (ListView) findViewById(R.id.lvMessage);
        listView.setVisibility(isVerbose ? View.VISIBLE : View.GONE);
    }

    // Recovers the cache files pair by pair, then clearing all of them after recovery
    private void recoverCachedFiles(final Context mContext) {
        Toast.makeText(this, "Recovering files", Toast.LENGTH_SHORT).show();
        final File folder = mContext.getCacheDir();

        // Get the cached files by prefix filters
        final File[] recoveredHrmFiles = folder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(final File dir,
                                  final String name) {
                return name.matches(getString(R.string.sessionCache_prefix) + mContext.getString(R.string.file_separator) + getString(R.string.hrm_cache_header) + ".*\\.tmp");
            }
        });

        final File[] recoveredLinAccFiles = folder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(final File dir,
                                  final String name) {
                return name.matches(getString(R.string.sessionCache_prefix) + mContext.getString(R.string.file_separator) + getString(R.string.linAcc_cache_header) + ".*\\.tmp");
            }
        });

        // Recovery by pair
        for (int i = 0; i < recoveredHrmFiles.length; i++) {
            final File hrmFile = recoveredHrmFiles[i];
            final File linAccFile = recoveredLinAccFiles[i];

            compileDataToJson(hrmFile, linAccFile, true, mContext);

        }
        // Remove the rest of the cache
        clearCache(mContext.getString(R.string.sessionCache_prefix), mContext);
    }

    @Override
    protected void onDestroy() {

        // Clean up connections
        if (mIsBound && mConsumerService != null) {
            if (!mConsumerService.closeConnection()) {
                updateTextView("Disconnected");

                setBtnEnable("Connect", true);
                setBtnEnable("Disconnect", false);
                setBtnEnable("Start", false);
                setBtnEnable("Stop", false);
                mMessageAdapter.clear();
            }
        }

        // Un-bind service
        if (mIsBound) {
            unbindService(mConnection);
            mIsBound = false;
        }
        super.onDestroy();
    }

    // Wrapper for XML interactions
    public void mOnClick(View v) {
        mOnClick(v, mContext);
    }

    // Actions when buttons are pressed
    private void mOnClick(View v, Context mContext) {
        switch (v.getId()) {
            case R.id.buttonConnect: {
                buttonConnectAction();
                break;
            }
            case R.id.buttonDisconnect: {
                buttonDisconnectAction();

                break;
            }
            case R.id.buttonStart: {
                buttonStartAction(true);

                break;
            }

            case R.id.buttonStop: {
                buttonStopAction();

                break;
            }
            case R.id.buttonStartRemote: {
                buttonStartRemoteAction();
                break;
            }

            case R.id.buttonStopRemote: {
                buttonStopRemoteAction();
                break;
            }

            default:
        }
    }


    // Cycles the activity
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, GraphActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    /**
     * Called when an item in the navigation menu is selected.
     *
     * @param item The selected item
     * @return true to display the item as the selected item
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_graph) {
            Intent intent = new Intent(this, GraphActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_setting) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private static final class Message {
        String data;

        public Message(String data) {
            super();
            this.data = data;
        }
    }

    private class MessageAdapter extends BaseAdapter {
        private static final int MAX_MESSAGES_TO_DISPLAY = 20;
        private List<Message> mMessages;

        public MessageAdapter() {
            mMessages = Collections.synchronizedList(new ArrayList<Message>());
        }

        void addMessage(final Message msg) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mMessages.size() == MAX_MESSAGES_TO_DISPLAY) {
                        mMessages.remove(0);
                        mMessages.add(msg);
                    } else {
                        mMessages.add(msg);
                    }
                    notifyDataSetChanged();
                    ((ListView) findViewById(R.id.lvMessage)).setSelection(getCount() - 1);
                }
            });
        }

        void clear() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMessages.clear();
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getCount() {
            return mMessages.size();
        }

        @Override
        public Object getItem(int position) {
            return mMessages.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View messageRecordView = null;
            if (inflator != null) {
                messageRecordView = inflator.inflate(R.layout.message, null);
                TextView tvData = messageRecordView.findViewById(R.id.tvData);
                Message message = (Message) getItem(position);
                tvData.setText(message.data);
            }
            return messageRecordView;
        }
    }
}
