/*
 * Copyright (c) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation and/or
 *       other materials provided with the distribution.
 *     * Neither the name of Samsung Electronics Co., Ltd. nor the names of its contributors may be used to endorse or
 *       promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.precise.robin.biostat;

import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.accessory.SA;
import com.samsung.android.sdk.accessory.SAAgent;
import com.samsung.android.sdk.accessory.SAPeerAgent;
import com.samsung.android.sdk.accessory.SASocket;

import java.io.IOException;
import java.util.List;

public class ConsoleService extends SAAgent {
    private static final String TAG = "BioStat";
    private static final Class<ServiceConnection> SASOCKET_CLASS = ServiceConnection.class;
    public static final int CONNECTION_RETRY_LIMIT = 30;
    public static final int RESTART_INTERVAL = 5000;
    private final IBinder mBinder = new LocalBinder();
    Handler mHandler = new Handler();
    private ServiceConnection mConnectionHandler = null;
    private CommandParser commandParser;

    public ConsoleService() {
        super(TAG, SASOCKET_CLASS);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SA mAccessory = new SA();
        try {
            mAccessory.initialize(this);
        } catch (SsdkUnsupportedException e) {
            // try to handle SsdkUnsupportedException
            if (processUnsupportedException(e)) {
                return;
            }
        } catch (Exception e1) {
            e1.printStackTrace();
            /*
             * Your application can not use Samsung Accessory SDK. Your application should work smoothly
             * without using this SDK, or you may want to notify user and close your application gracefully
             * (release resources, stop Service threads, close UI thread, etc.)
             */
            stopSelf();
        }

        commandParser = new CommandParser(getApplicationContext());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    protected void onFindPeerAgentsResponse(SAPeerAgent[] peerAgents, int result) {
        if ((result == SAAgent.PEER_AGENT_FOUND) && (peerAgents != null)) {
            for (SAPeerAgent peerAgent : peerAgents)
                requestServiceConnection(peerAgent);
        } else if (result == SAAgent.FINDPEER_DEVICE_NOT_CONNECTED) {
            Toast.makeText(getApplicationContext(), "FINDPEER_DEVICE_NOT_CONNECTED", Toast.LENGTH_LONG).show();
            updateTextView("Disconnected");

            setBtnEnable("Connect", true);
            setBtnEnable("Disconnect", false);
            setBtnEnable("Start", false);
            setBtnEnable("StartRemote", false);
            setBtnEnable("Stop", false);
            setBtnEnable("StopRemote", false);

        } else if (result == SAAgent.FINDPEER_SERVICE_NOT_FOUND) {
            Toast.makeText(getApplicationContext(), "FINDPEER_SERVICE_NOT_FOUND", Toast.LENGTH_LONG).show();
            updateTextView("Disconnected");

            setBtnEnable("Connect", true);
            setBtnEnable("Disconnect", false);
            setBtnEnable("Start", false);
            setBtnEnable("StartRemote", false);
            setBtnEnable("Stop", false);
            setBtnEnable("StopRemote", false);
        } else {
            Toast.makeText(getApplicationContext(), R.string.NoPeersFound, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onServiceConnectionRequested(SAPeerAgent peerAgent) {
        if (peerAgent != null) {
            acceptServiceConnectionRequest(peerAgent);
        }
    }

    @Override
    protected void onServiceConnectionResponse(SAPeerAgent peerAgent, SASocket socket, int result) {
        if (result == SAAgent.CONNECTION_SUCCESS) {
            this.mConnectionHandler = (ServiceConnection) socket;
            updateTextView("Connected");

            setBtnEnable("Connect", false);
            setBtnEnable("Disconnect", true);
            setBtnEnable("Start", true);
            setBtnEnable("Stop", false);

            if (ConsoleActivity.isRemote) {
                setBtnEnable("StartRemote", false);
                setBtnEnable("StopRemote", true);
            } else {
                setBtnEnable("StartRemote", true);
                setBtnEnable("StopRemote", false);
            }


        } else if (result == SAAgent.CONNECTION_ALREADY_EXIST) {
            updateTextView("Connected");

            setBtnEnable("Connect", false);
            setBtnEnable("Disconnect", true);
            setBtnEnable("Start", true);
            setBtnEnable("Stop", false);
            if (ConsoleActivity.isRemote) {
                setBtnEnable("StartRemote", false);
                setBtnEnable("StopRemote", true);
            } else {
                setBtnEnable("StartRemote", true);
                setBtnEnable("StopRemote", false);
            }
            Toast.makeText(getBaseContext(), "CONNECTION_ALREADY_EXIST", Toast.LENGTH_LONG).show();
        } else if (result == SAAgent.CONNECTION_DUPLICATE_REQUEST) {
            Toast.makeText(getBaseContext(), "CONNECTION_DUPLICATE_REQUEST", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getBaseContext(), R.string.ConnectionFailure, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onError(SAPeerAgent peerAgent, String errorMessage, int errorCode) {
        super.onError(peerAgent, errorMessage, errorCode);
    }

    @Override
    protected void onPeerAgentsUpdated(SAPeerAgent[] peerAgents, int result) {
        final SAPeerAgent[] peers = peerAgents;
        final int status = result;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (peers != null) {
                    if (status == SAAgent.PEER_AGENT_AVAILABLE) {
                        Toast.makeText(getApplicationContext(), "PEER_AGENT_AVAILABLE", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "PEER_AGENT_UNAVAILABLE", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    // Pass the data to be graphed and cached
    private void parseData(byte[] data) {
        final JSonParser parser = new JSonParser(new String(data));
        if (parser.isData()) {
            final List<DataObject> jsonList = parser.getJsonList();
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    GraphActivity.processData(jsonList);

                    ConsoleActivity.cacheData(jsonList);

                }
            });
        }
        if (parser.isCommand()) {

            commandParser.parse(parser.getCommandList());
        }


    }

    public void findPeers() {
        findPeerAgents();
    }

    public boolean sendData(final String data) {
        boolean retvalue = false;
        if (mConnectionHandler != null) {
            try {
                mConnectionHandler.send(getServiceChannelId(0), data.getBytes());
                retvalue = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            addMessage("Sent: ", data);
        }
        return retvalue;
    }

    public boolean closeConnection() {

        if (mConnectionHandler != null) {
            mConnectionHandler.close();
            mConnectionHandler = null;
            return true;
        } else {
            return false;
        }
    }

    private boolean processUnsupportedException(SsdkUnsupportedException e) {
        e.printStackTrace();
        int errType = e.getType();
        if (errType == SsdkUnsupportedException.VENDOR_NOT_SUPPORTED
                || errType == SsdkUnsupportedException.DEVICE_NOT_SUPPORTED) {
            /*
             * Your application can not use Samsung Accessory SDK. You application should work smoothly
             * without using this SDK, or you may want to notify user and close your app gracefully (release
             * resources, stop Service threads, close UI thread, etc.)
             */
            stopSelf();
        } else if (errType == SsdkUnsupportedException.LIBRARY_NOT_INSTALLED) {
            Log.e(TAG, "You need to install Samsung Accessory SDK to use this application.");
        } else if (errType == SsdkUnsupportedException.LIBRARY_UPDATE_IS_REQUIRED) {
            Log.e(TAG, "You need to update Samsung Accessory SDK to use this application.");
        } else if (errType == SsdkUnsupportedException.LIBRARY_UPDATE_IS_RECOMMENDED) {
            Log.e(TAG, "We recommend that you update your Samsung Accessory SDK before using this application.");
            return false;
        }
        return true;
    }

    private void updateTextView(final String str) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                ConsoleActivity.updateTextView(str);
            }
        });
    }

    private void addMessage(final String prefix, final String data) {
        final String strToUI = prefix.concat(data);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                ConsoleActivity.addMessage(strToUI);
            }
        });
    }

    private void setBtnEnable(final String id, final boolean state) {

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                ConsoleActivity.setBtnEnable(id, state);
            }
        });
    }

    public class ServiceConnection extends SASocket {

        private int mInterval = RESTART_INTERVAL;

        private Runnable mStatusChecker = new Runnable() {
            private int retryLimit = CONNECTION_RETRY_LIMIT;

            @Override
            public void run() {
                if (!ConsoleActivity.isBusy && retryLimit > 0) {
                    Log.i("Debug", "Retry limit: " + Integer.toString(retryLimit));
                    updateTextView("Reconnecting: Check phone bluetooth and watch battery");
                    // Restart
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            ConsoleActivity.performReconnect(false);
                        }
                    });
                    // If reconnection achieved then stop reconnection attempts
                    if (ConsoleActivity.isBusy) {
                        stopReconnection();
                        Log.i("DEBUG", "Service is running");

                    } else {
                        // Else wasted a reconnection attempt
                        retryLimit--;
                        Log.i("DEBUG", "In not busy statement");
                        // If still more attempts left, rerun
                        if (retryLimit > 0) {
                            Log.i("DEBUG", "DELAY STATEMENT");
                            mHandler.postDelayed(mStatusChecker, mInterval);
                        } else {
                            // Since still not connected and ran out of attempts, give up and compile the cache to JSON, enable compilation with isBusy
                            disconnect("Disconnected: reconnecting failed", true);
                        }

                    }
                } else {
                    Log.i("Debug", "Runner stopped: bool:" + Boolean.toString(ConsoleActivity.isBusy) + " limit: " + Integer.toString(retryLimit));
                }


            }
        };

        public ServiceConnection() {
            super(ServiceConnection.class.getName());
        }

        @Override
        public void onError(int channelId, String errorMessage, int errorCode) {
        }

        @Override
        public void onReceive(int channelId, byte[] data) {
            final String message = new String(data);
            addMessage("Received: ", message);
            parseData(data);
        }

        @Override
        protected void onServiceConnectionLost(int reason) {
            // Reason: android bluetooth is off or watch if off
            if (reason == CONNECTION_LOST_DEVICE_DETACHED) {
                if (ConsoleActivity.isBusy && !ConsoleActivity.isRemote) {
                    ConsoleActivity.isBusy = false;

                    // Start in proper reconnection button setting
                    setBtnEnable("Connect", false);
                    setBtnEnable("Disconnect", false);
                    setBtnEnable("Start", false);
                    setBtnEnable("Stop", false);

                    startReconnection();
                } else {
                    // Was only connected, just disconnect
                    disconnect();
                }

            } else if (reason == CONNECTION_LOST_PEER_DISCONNECTED) {
                // Reason: regular disconnect from wearable on request
                disconnect();
            } else {
                // Reason: unexpected reason
                disconnect("Disconnected with reason: " + Integer.toString(reason));
            }
        }

        // Helper functions for checking if consecutive clicks are performed within the interval for resetting the setting values to default
        private void startReconnection() {

            mStatusChecker.run();
        }

        private void stopReconnection() {
            mHandler.removeCallbacks(mStatusChecker);
        }

        private void disconnect() {
            disconnect("Disconnected");
        }

        private void disconnect(String msg) {
            disconnect(msg, false);
        }

        private void disconnect(String msg, boolean isLeftOver) {
            updateTextView(msg);

            setBtnEnable("Connect", true);
            setBtnEnable("Disconnect", false);
            setBtnEnable("Start", false);
            setBtnEnable("StartRemote", false);
            setBtnEnable("Stop", false);
            setBtnEnable("StopRemote", false);

            // If cannot reconnect, compile and disconnect
            if (!ConsoleActivity.isRemote && (ConsoleActivity.isBusy || isLeftOver)) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        ConsoleActivity.compileDataToJson(getApplicationContext());

                    }
                });
            }
            closeConnection();
        }
    }

    public class LocalBinder extends Binder {
        public ConsoleService getService() {
            return ConsoleService.this;
        }
    }

}
