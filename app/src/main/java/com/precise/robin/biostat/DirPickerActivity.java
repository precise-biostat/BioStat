package com.precise.robin.biostat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import lib.folderpicker.FolderPicker;

public class DirPickerActivity extends AppCompatActivity {

    private static final int FOLDERPICKER_CODE = 12;
    private String key;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, FolderPicker.class);
        //To show a custom title
        intent.putExtra("title", "Select file to upload");

        //To begin from a selected folder instead of sd card's root folder
        intent.putExtra("location", getIntent().getStringExtra("LOCATION"));

        //To pick files
        intent.putExtra("pickFiles", false);

        // Save which directory key to be modified
        key = getIntent().getStringExtra("KEY");

        // Start directory folder picker
        startActivityForResult(intent, FOLDERPICKER_CODE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == FOLDERPICKER_CODE && resultCode == Activity.RESULT_OK) {

            // Pass the returned data of the new directory to the summary of the designated preference summary by the key
            final String folderLocation = intent.getExtras().getString("data");
            if (folderLocation != null) {
                Log.i("folderLocation", folderLocation);
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        SettingsActivity.setSummary(key, folderLocation, getApplicationContext());
                    }
                });
            }

        }
        this.finish();
    }

}
