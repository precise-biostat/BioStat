package com.precise.robin.biostat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Object that contains the parameters for HRM data
public class HrmDataObject extends DataObject {
    private List<String> params = new ArrayList<>(Arrays.asList("intensity", "pedomStatus", "pedomTotalStep"));

    public HrmDataObject(String s) throws IllegalArgumentException {
        super(s);

        for (String param : params) {
            if (!super.getJson().has(param)) {
                throw new IllegalArgumentException();
            }
        }

        params.addAll(super.getParams());
    }

    @Override
    public List<String> getParams() {
        return new ArrayList<>(params);
    }

    public long getIntensity() {
        return super.getJson().getAsJsonPrimitive("intensity").getAsLong();
    }

    public int getPedomTotalStep() {
        return super.getJson().getAsJsonPrimitive("pedomTotalStep").getAsInt();
    }

    public String getPedomStatus() {
        return super.getJson().getAsJsonPrimitive("pedomStatus").getAsString();
    }
}
