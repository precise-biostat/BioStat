package com.precise.robin.biostat;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.io.FilenameFilter;

import static android.view.KeyEvent.KEYCODE_MENU;

public class SettingsActivity extends AppCompatPreferenceActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 0;
    public static SettingsActivity instance;
    public static boolean disablePrefixEditing = false;
    private static int reset_counter;
    private static int mInterval = 5000;
    private static Handler mHandler;
    private static int clickLimit = 2;
    static Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                // Check if need to reset counter since time interval has passed
                if (reset_counter < clickLimit) {
                    reset_counter = 0;
                }
            } finally {
                // Otherwise run again after interval to check
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };
    private static Preference hrmFileDir, linFileDir;
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            String stringValue = newValue.toString();
            preference.setSummary(stringValue);
            return true;
        }
    };

    // Helper functions for checking if consecutive clicks are performed within the interval for resetting the setting values to default
    static void startRepeatingTask() {
        mStatusChecker.run();
    }

    static void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }

    // Sets the summary of a preference by the key value
    public static void setSummary(String key, String summary, Context mContext) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = shared.edit();

        if (key.equals(mContext.getString(R.string.key_hrm_dir)) && hrmFileDir != null) {
            hrmFileDir.setSummary(summary);
            editor.putString(key, summary);
        } else if (key.equals(mContext.getString(R.string.key_lin_dir)) && linFileDir != null) {
            linFileDir.setSummary(summary);
            editor.putString(key, summary);
        }
        editor.commit();
    }

    // Remove all of the modified setting, will load deault setting from XML
    public static void clearSettings(Context mContext) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    private static void bindPreferenceSummaryToValue(Preference preference) {
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    public static SettingsActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        reset_counter = 0;
        mHandler = new Handler();

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_setting);
        instance = this;
        // Action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Prompt for permission
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            permissionPopUp();
        }

        // Hint about setting usage
        Toast.makeText(getApplicationContext(), R.string.Setting_changed_notification, Toast.LENGTH_SHORT).show();

        // Load settings fragment
        getFragmentManager().beginTransaction().replace(R.id.setting_content, new MainPreferenceFragment()).commit();

        // Auto reconnect if Android app crashes
        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));

        // Check if previous run of the app was incomplete, recovers the data if possible, and restart connection
        Intent intent = new Intent(this, ConsoleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        // Add extras
        // Extras for when app crashed with leftover cache
        boolean isRecoverable = checkLeftOverDataCache();

        boolean isCrash = getIntent().getBooleanExtra("crash", false);
        if (isRecoverable && !isCrash) {
            intent.putExtra("isRecovery", true);
        }


        if (isCrash && isRecoverable) {
            Toast.makeText(this, "App restarted after crash, restarting connection!", Toast.LENGTH_SHORT).show();
            // Restart connection
            intent.putExtra("isReconnect", true);
        }


        // Start Console activity if contains extras
        if (intent.hasExtra("isRecovery") || intent.hasExtra("isReconnect")) {
            startActivity(intent);
        }
    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    // Creates the prompt for granting permission
    private void permissionPopUp() {
        ActivityCompat.requestPermissions(SettingsActivity.this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
    }

    // Disable the menu key
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return keyCode == KEYCODE_MENU || super.onKeyDown(keyCode, event);

    }

    // Cycles the activities
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ConsoleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    // Checks for residual cache in cache directory, needs to be a part of the main activity so that prefix is consistent when checking, return if cache detected
    private boolean checkLeftOverDataCache() {

        final File folder = getApplicationContext().getCacheDir();

        // Check if cache is empty
        final File[] files = folder.listFiles();
        if (files.length > 0) {
            // Get all of files to be recovered
            final File[] recoveredHrmFiles = folder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(final File dir,
                                      final String name) {
                    return name.matches(getString(R.string.sessionCache_prefix) + "_" + getString(R.string.hrm_cache_header) + ".*\\.tmp");
                }
            });

            final File[] recoveredLinAccFiles = folder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(final File dir,
                                      final String name) {
                    return name.matches(getString(R.string.sessionCache_prefix) + "_" + getString(R.string.linAcc_cache_header) + ".*\\.tmp");
                }
            });

            final int hrm = recoveredHrmFiles.length;
            int lin = recoveredLinAccFiles.length;

            // Check if there is a valid quantity of cache files
            if (hrm > 0 || lin > 0) {
                Toast.makeText(this, "Detected leftover cache!\nHRM files: " + Integer.toString(hrm) + "\nLIN_ACC files: " + Integer.toString(lin), Toast.LENGTH_SHORT).show();

                // Check if there is an even number of the files, if so then we can recover, else delete everything with prefix (logic in ConsoleActivity onCreate)
                if (hrm == lin) {
                    disablePrefixEditing = true;
                    return true;

                }

            }
        }

        return false;

    }

    /**
     * Called when an item in the navigation menu is selected.
     *
     * @param item The selected item
     * @return true to display the item as the selected item
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_console) {

            Intent intent = new Intent(this, ConsoleActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_graph) {
            Intent intent = new Intent(this, GraphActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public static class MainPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_main);


            // Bind the summary to value for the Prefix preferences
            Preference hrmPrefixPref = findPreference(getString(R.string.key_hrm_prefix));
            bindPreferenceSummaryToValue(hrmPrefixPref);

            Preference linPrefixPref = findPreference(getString(R.string.key_lin_acc_prefix));
            bindPreferenceSummaryToValue(linPrefixPref);


            // Enable / Disable prefix editing to persist stat for data recovery

            if (disablePrefixEditing || !getIsVerbose()) {
                hrmPrefixPref.setEnabled(false);
                linPrefixPref.setEnabled(false);
            } else {
                hrmPrefixPref.setEnabled(true);
                linPrefixPref.setEnabled(true);
            }

            // Get the default app file directory
            final ContextWrapper c = new ContextWrapper(getActivity());
            final String appPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + c.getPackageName();

            // Setting the hrm file dir preference
            bindPreferenceSummaryToValue(findPreference(getString(R.string.key_hrm_dir)));
            hrmFileDir = findPreference(getString(R.string.key_hrm_dir));
            final File hrmDirectory = createFolder(appPath + File.separator + c.getString(R.string.hrm_folder_name));

            // Check if need to direct to actual directory
            if (hrmFileDir.getSummary().equals("") || hrmFileDir.getSummary().equals(getString(R.string.default_hrm_storage))) {
                setSummary(getString(R.string.key_hrm_dir), hrmDirectory.getAbsolutePath(), getActivity());
            }

            // Send intent when clicked to open folder picker
            hrmFileDir.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent intent = new Intent(getActivity(), DirPickerActivity.class);
                    intent.putExtra("KEY", getString(R.string.key_hrm_dir));
                    intent.putExtra("LOCATION", hrmFileDir.getSummary());
                    startActivity(intent);
                    return true;
                }
            });

            // Setting the linear acceleration file dir preference
            bindPreferenceSummaryToValue(findPreference(getString(R.string.key_lin_dir)));
            linFileDir = findPreference(getString(R.string.key_lin_dir));
            final File linAccDirectory = createFolder(appPath + File.separator + c.getString(R.string.lin_acc_folder_name));

            // Check if need to direct to actual directory
            if (linFileDir.getSummary().equals("") || linFileDir.getSummary().equals(getString(R.string.default_lin_acc_storage))) {

                setSummary(getString(R.string.key_lin_dir), linAccDirectory.getAbsolutePath(), getActivity());
            }

            // Send intent when clicked to open folder picker
            linFileDir.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent intent = new Intent(getActivity(), DirPickerActivity.class);
                    intent.putExtra("KEY", getString(R.string.key_lin_dir));
                    intent.putExtra("LOCATION", linFileDir.getSummary());
                    startActivity(intent);
                    return true;
                }
            });

            // Set up reset preference for 2 (clickLimit) clicks within 5 seconds (mInterval), else reset the clicks
            Preference resetPref = findPreference(getString(R.string.reset_key));

            resetPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if (reset_counter == 0) {
                        startRepeatingTask();
                    }

                    reset_counter++;
                    if (reset_counter < clickLimit) {
                        Toast.makeText(getActivity(), R.string.click_reset_hint, Toast.LENGTH_SHORT).show();
                    } else if (reset_counter >= clickLimit) {
                        // Clear the settings
                        clearSettings(getActivity());
                        Toast.makeText(getActivity(), R.string.value_reset_complete, Toast.LENGTH_SHORT).show();

                        // Remove the runnable and reset the values and layout, restart the activity
                        reset_counter = 0;
                        stopRepeatingTask();
                        onDestroy();
                        onCreate(savedInstanceState);
                    }

                    return true;
                }
            });

            CheckBoxPreference verbosePref = (CheckBoxPreference) findPreference(getString(R.string.verbose_mode_key));
            verbosePref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                                                         @Override
                                                         public boolean onPreferenceClick(Preference preference) {
                                                             onDestroy();
                                                             onCreate(savedInstanceState);
                                                             return true;
                                                         }
                                                     }

            );
            updateVerboseView();


            final CheckBoxPreference remoteSessionPref = (CheckBoxPreference) findPreference(getString(R.string.remote_session_key));
            remoteSessionPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    // Under what situation to allow for changing between remote and live sessions
                    // TODO: maybe check if currently an existing connection before switching, if storing  log of connections
                    if (ConsoleActivity.isBusy) {
                        // If busy, then toast to notify that change only when not busy
                        Toast.makeText(getActivity(), "Changing between remote and live session mode only allowed when current session is finished", Toast.LENGTH_LONG).show();
                        // Persist current state
                        remoteSessionPref.setChecked(!remoteSessionPref.isChecked());

                    }

                    return true;
                }
            });

            // Bind the summary to value for Patient ID
            final Preference patientIdPref = findPreference(getString(R.string.patient_name_key));

            Preference.OnPreferenceChangeListener patientIdChangeListener = new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String stringValue = newValue.toString();
                    preference.setSummary(stringValue);

                    if (stringValue.equals("")) {
                        // Set default directories
                        setSummary(getString(R.string.key_hrm_dir), hrmDirectory.getAbsolutePath(), getActivity());
                        setSummary(getString(R.string.key_lin_dir), linAccDirectory.getAbsolutePath(), getActivity());
                    } else {
                        // Else set both as a folder named UserID
                        File newUserHrmDirectory = createFolder(appPath + File.separator + stringValue + File.separator + c.getString(R.string.hrm_folder_name));
                        File newUserLinDirectory = createFolder(appPath + File.separator + stringValue + File.separator + c.getString(R.string.lin_acc_folder_name));
                        setSummary(getString(R.string.key_hrm_dir), newUserHrmDirectory.getAbsolutePath(), getActivity());
                        setSummary(getString(R.string.key_lin_dir), newUserLinDirectory.getAbsolutePath(), getActivity());

                    }


                    return true;
                }
            };
            // Bind change listener
            patientIdPref.setOnPreferenceChangeListener(patientIdChangeListener);

            // Force update on change listener to start
            patientIdChangeListener.onPreferenceChange(patientIdPref,
                    PreferenceManager
                            .getDefaultSharedPreferences(patientIdPref.getContext())
                            .getString(patientIdPref.getKey(), ""));

        }

        private File createFolder(String pathName) {
            // Create new folder of UserID and set as all file directories
            final File directory = new File(pathName);

            // Check if directory exist, if not create
            if (!directory.isDirectory()) {
                directory.mkdirs();
            }

            return directory;
        }

        private void updateVerboseView() {
            Log.i("DEBUG", "Verbose update");
            if (!getIsVerbose()) {
                Preference hrmPrefixPref = findPreference(getString(R.string.key_hrm_prefix));
                disablePreference(hrmPrefixPref);
                Preference hrmDirPref = findPreference(getString(R.string.key_hrm_dir));
                disablePreference(hrmDirPref);
                Preference linAccPrefixPref = findPreference(getString(R.string.key_lin_acc_prefix));
                disablePreference(linAccPrefixPref);
                Preference linAccDirPref = findPreference(getString(R.string.key_lin_dir));
                disablePreference(linAccDirPref);


                Preference refreshRatePref = findPreference(getString(R.string.refresh_rate_key));
                disablePreference(refreshRatePref);

                Preference watchLocalStoragePref = findPreference(getString(R.string.key_watch_local_storage));
                disablePreference(watchLocalStoragePref);

                Preference demoRatePref = findPreference(getString(R.string.demo_dynamic_rate_key));
                disablePreference(demoRatePref);
            }
        }

        private void disablePreference(Preference pref) {
            pref.setSelectable(false);
            pref.setEnabled(false);
        }

        private boolean getIsVerbose() {
            CheckBoxPreference verbosePref = (CheckBoxPreference) findPreference(getString(R.string.verbose_mode_key));
            return verbosePref.isChecked();
        }

        // Make sure to update the prefix prefernces after recovery is done
        @Override
        public void onResume() {

            Preference hrmPrefixPref = findPreference(getString(R.string.key_hrm_prefix));
            bindPreferenceSummaryToValue(hrmPrefixPref);

            Preference linPrefixPref = findPreference(getString(R.string.key_lin_acc_prefix));
            bindPreferenceSummaryToValue(linPrefixPref);

            if (disablePrefixEditing || !getIsVerbose()) {
                hrmPrefixPref.setEnabled(false);
                linPrefixPref.setEnabled(false);
            } else {
                hrmPrefixPref.setEnabled(true);
                linPrefixPref.setEnabled(true);
            }
            super.onResume();
        }
    }

}